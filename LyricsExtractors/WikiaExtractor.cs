﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using LyricsGetters.exceptions;

namespace LyricsGetters {
    public class WikiaExtractor {
        private int timeout;

        public WikiaExtractor(int timeout) {
            this.timeout = timeout;
        }

        /// <summary>
        /// Get the lyrics from wikia
        /// </summary>
        /// <param name="Song"></param>
        /// <param name="Artist"></param>
        /// <returns></returns>
        public String getLyrics(String Song, String Artist) {
            String URLString = ("http://lyrics.fandom.com/" + Artist + ":" + Song);
            String htmlCode;
            // Download the html source code
            using (MyWebClient client = new MyWebClient(timeout)) {
                // client.DownloadFile("http://yoursite.com/page.html", @"C:\localfile.html");
                try {
                    // Or you can get the file content without saving it:
                    htmlCode = client.DownloadString(URLString).ToString();
                } catch (WebException e) {
                    // Check 404 errors
                    if ((e.Response is System.Net.HttpWebResponse) && (e.Response as System.Net.HttpWebResponse).StatusCode == System.Net.HttpStatusCode.NotFound)
                        throw new LyricNotFoundException();
                    // Check if request timed out
                    if (e.Status == WebExceptionStatus.Timeout)
                        throw new LyricsRequestTimedOutException();
                    //Console.WriteLine("LyricsGetters::WebError::" + Song + ":" + Artist + "::" + e.Message);
                    // Otherwise
                    throw new GenericWebErrorException(e);
                }
            }
            return isolateLyricsFromHtml(htmlCode);
        }

        /// <summary>
        ///  Isolate the lyrics from a lyric.wikia HTML
        /// </summary>
        /// <param name="HTML"></param>
        /// <returns></returns>
        private String isolateLyricsFromHtml(String htmlCode) {
            // MAGIC CHARACTER :: "</script>&#" or "</script><b>&#"
            // Let's start to remove code before lyrics
            //File.WriteAllText("test.txt", htmlCode);
            if (htmlCode.IndexOf("<div class='lyricbox'>") > 0) {
                int c = htmlCode.IndexOf("<div class='lyricbox'>");
                htmlCode = htmlCode.Remove(0, c);
            } else throw new LyricNotFoundException();
            htmlCode = htmlCode.Remove(htmlCode.IndexOf("<div class='lyricsbreak'>"));
            /*
            if (htmlCode.IndexOf("</script>&#") > 0) {
                int c = htmlCode.IndexOf("</script>&#");
                htmlCode = htmlCode.Remove(0, c + 9);
            } else if (htmlCode.IndexOf("</script><b>&#") > 0) {
                int c = htmlCode.IndexOf("</script><b>&#");
                htmlCode = htmlCode.Remove(0, c + 12);
            } else {
                return null;
            }
            
            // Let's remove text after lyrics
            htmlCode = htmlCode.Remove(htmlCode.IndexOf("<p>NewPP") - 7);
            */
            return htmlCode.Trim();
        }
    }
}
