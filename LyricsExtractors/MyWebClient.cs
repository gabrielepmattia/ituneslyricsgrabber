﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;


namespace LyricsGetters {

    class MyWebClient : WebClient {
        private readonly int mTimeout;
        private readonly bool mLessWin7;
        public MyWebClient(int timeout) {
            this.mTimeout = timeout;
            // Check windows version
            this.mLessWin7 = DotNetWindowsInfo.SystemInfo.GetInfo().CurrentBuild < 8000;
        }
        protected override WebRequest GetWebRequest(Uri uri) {
            if (this.mLessWin7) {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                       | SecurityProtocolType.Tls11
                                                       | SecurityProtocolType.Tls12
                                                       | SecurityProtocolType.Ssl3;
            }
            
            WebRequest w = base.GetWebRequest(uri);
            w.Timeout = mTimeout;
            return w;
        }
    }
}
