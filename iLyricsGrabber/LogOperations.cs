﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using iTunesLib;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using System.Xml;

using System.Globalization;
using System.Deployment.Application;
using System.Reflection;

namespace iLyricsGrabber
{
    public class LogOperations : Main
    {
        public static void Log(string logMessage, TextWriter w)
        {
            //  w.Write("\r\nCreated Session : ");
            w.WriteLine("{0} {1}::: {2}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString(), logMessage);
            //  w.WriteLine("  :");
            //  w.WriteLine("  :{0}", logMessage);
            //  w.WriteLine("-------------------------------");
        }

        public LogOperations()
        {

            using (StreamWriter w = File.AppendText("log.txt"))
            {
                w.WriteLine("");
                w.WriteLine("");
                w.WriteLine("--> iTunesLyricsGrabber by gabry3795 - Log File || Debug v.{0} ", Application.ProductVersion);
                w.WriteLine("--> If you have problems send this file to gabry.gabry@hotmail.it. Don't modify this file");
                w.WriteLine("--------------------------------------------------------------------------------");
                w.WriteLine("TA{0} || MTA{1} || ovw{2} || SN{3} ", TA.ToString(), MTA.ToString(), ovw.ToString(), SN.ToString());
                w.WriteLine("--------------------------------------------------------------------------------");
                //  Log(" ", w);
                //  Log("--------------------------------------------------------------------------------", w);
                Log("Started session", w);
            }

            using (StreamReader r = File.OpenText("log.txt"))
            {
                DumpLog(r);
            }
        }


   }


    }