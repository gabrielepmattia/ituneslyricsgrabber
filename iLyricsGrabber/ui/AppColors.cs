﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace iLyricsGrabber.ui {
    class AppColors {
        public static Color DarkBlue = ColorTranslator.FromHtml("#01141f");
        public static Color LightBlue = ColorTranslator.FromHtml("#0089d8");
    }
}
