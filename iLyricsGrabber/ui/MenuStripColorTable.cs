﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace iLyricsGrabber.ui {
    public class MenuStripColorTable : ProfessionalColorTable {
        public override Color MenuItemSelected {
            get { return System.Drawing.ColorTranslator.FromHtml("#0089d8"); }
        }

        public override Color MenuBorder {
            get { return System.Drawing.ColorTranslator.FromHtml("#0089d8"); }
        }

        public override Color MenuStripGradientBegin {
            get { return System.Drawing.ColorTranslator.FromHtml("#01141f"); }
        }

        public override Color MenuStripGradientEnd {
            get { return System.Drawing.ColorTranslator.FromHtml("#01141f"); }
        }

        public override Color MenuItemSelectedGradientBegin {
            get { return System.Drawing.ColorTranslator.FromHtml("#01141f"); }
        }

        public override Color MenuItemSelectedGradientEnd {
            get { return System.Drawing.ColorTranslator.FromHtml("#01141f"); }
        }

        public override Color MenuItemPressedGradientBegin {
            get { return System.Drawing.ColorTranslator.FromHtml("#01141f"); }
        }

        public override Color MenuItemPressedGradientEnd {
            get { return System.Drawing.ColorTranslator.FromHtml("#01141f"); }
        }

        public override Color OverflowButtonGradientBegin {
            get { return System.Drawing.ColorTranslator.FromHtml("#01141f"); }
        }

        public override Color OverflowButtonGradientEnd {
            get { return System.Drawing.ColorTranslator.FromHtml("#01141f"); }
        }

        public override Color OverflowButtonGradientMiddle {
            get { return System.Drawing.ColorTranslator.FromHtml("#01141f"); }
        }

        public override Color ToolStripContentPanelGradientBegin {
            get { return System.Drawing.ColorTranslator.FromHtml("#01141f"); }
        }

        public override Color ToolStripContentPanelGradientEnd {
            get { return System.Drawing.ColorTranslator.FromHtml("#01141f"); }
        }

        public override Color ToolStripDropDownBackground {
            get { return System.Drawing.ColorTranslator.FromHtml("#01141f"); }
        }

        public override Color ImageMarginGradientBegin {
            get { return System.Drawing.ColorTranslator.FromHtml("#01141f"); }
        }

        public override Color ImageMarginGradientEnd {
            get { return System.Drawing.ColorTranslator.FromHtml("#01141f"); }
        }

        public override Color ImageMarginGradientMiddle {
            get { return System.Drawing.ColorTranslator.FromHtml("#01141f"); }
        }

        public override Color SeparatorDark {
            get { return Color.White; }
        }

        public override Color SeparatorLight {
            get { return System.Drawing.ColorTranslator.FromHtml("#01141f"); }
        }
    }
}
