﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace iLyricsGrabber {
    static class Program {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main() {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Splashscreen
            Starting.ShowSplashScreen();
            Main mainForm = new Main(); //this takes ages
            Starting.CloseForm();
            Application.Run(mainForm);
        }
    }
}
