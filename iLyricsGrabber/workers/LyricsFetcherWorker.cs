﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTunesLib;
using System.Threading;
using System.Windows.Forms;
using System.ComponentModel;
using System.IO;

namespace iLyricsGrabber.workers {
    class LyricsFetcherWorker {
        private iTunesApp oItunes;
        private Thread[] threads_array;     // Implements the ThreadPool
        private static int lyrics_not_found;
        private static int lyrics_errors;
        private int total_tracks;
        private static int current_track_index;
        Main main_form = (Main)Application.OpenForms["Main"];
        // Lockers
        private static Object counterLocker = new Object();

        public LyricsFetcherWorker(iTunesApp oItunes) {
            this.oItunes = oItunes;
            this.threads_array = new Thread[Properties.Settings.Default.MaxParallelWorks];
            this.total_tracks = oItunes.SelectedTracks.Count;
            lyrics_not_found = 0;
            lyrics_errors = 0;
            current_track_index = 0;
        }

        public void DoWork() {
            try {
                main_form.prepareView();

                // Temporization for Table Update
                Thread.Sleep(500);

                // Get the tracks
                iTunesLib.IITTrackCollection selected_tracks = oItunes.SelectedTracks;

                // Cycle every track
                foreach (IITTrack track_raw in selected_tracks) {
                    // Temporization for Table Update
                    Thread.Sleep(50);
                    // If we have to add lyrics to track it must be of type IITFileOrCDTrack, if not track is
                    // not available or it's not a track.
                    if(!(track_raw is IITFileOrCDTrack)) { 
                        main_form.newSongQueue(current_track_index, track_raw.Name, track_raw.Artist, track_raw.Album);
                        main_form.updateStatusForSong(track_raw.Name, track_raw.Artist, current_track_index, Main.STATUS_TRACK_ERROR);
                        current_track_index++;
                        lyrics_errors++;
                        continue;
                    }
                    // If we are here, track is valid
                    IITFileOrCDTrack track = (IITFileOrCDTrack)track_raw;
                    // First, add track to table
                    main_form.newSongQueue(current_track_index, track.Name, track.Artist, track.Album);
                    // Check if song has already lyrics
                    if (!Properties.Settings.Default.Overwrite && track.Lyrics != null) {
                        main_form.updateStatusForSong(track.Name, track.Artist, current_track_index, Main.STATUS_TRACK_SKIPPED);
                        current_track_index++;
                        continue;
                    }
                    // Check if there is a free slot
                    int slot;
                    while (true) {
                        slot = getFirstFreeThreadSlot();
                        if (slot != -1) break;
                    }

                    SongLyricsFetcherWorker songLyricsFetcherWorker = new SongLyricsFetcherWorker(track, current_track_index);
                    Thread songLyricsFetcherThread = new Thread(songLyricsFetcherWorker.DoWork);
                    songLyricsFetcherThread.SetApartmentState(ApartmentState.STA);
                    threads_array[slot] = songLyricsFetcherThread;
                    threads_array[slot].Start();
                    current_track_index++;
                }
                // My join function
                while (true) if (getTotalFreeThreadSlots() == threads_array.Length) break;
            } catch (NullReferenceException) {
                MessageBox.Show("There was an error when processing your selected tracks, please make sure that:\n- you have selected at least 1 item;\n- iTunes doesn't require your attention to go on.", "Error accessing selected tracks", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            } catch (InvalidCastException) {
                // Track type is not valid
                MessageBox.Show("There was an error when processing your selected tracks, please make sure that:\n- all tracks that you have selected are downloaded on this pc (not only in Cloud)\n- you have selected at least 1 item;\n- iTunes doesn't require your attention to go on.", "Error accessing selected tracks", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            } catch (ThreadAbortException) {
                // Thread was aborted we have to abort every single thread in the pool
                //main_form.updateStatusForSong("--", "--", current_track_index, Main.STATUS_ABORTED);
                abortAllThreads();
            } finally {
                main_form.cleanView();
            }
        }

        /// <summary>
        /// Find the first free slot in the ThreadPool and return it, else -1
        /// </summary>
        /// <returns></returns>
        private int getFirstFreeThreadSlot() {
            for (int i = 0; i < threads_array.Length; i++) {
                if (threads_array[i] == null || !threads_array[i].IsAlive) return i;
            }
            return -1;
        }

        /// <summary>
        /// Count total free slots, to use as a join function for example
        /// </summary>
        /// <returns></returns>
        private int getTotalFreeThreadSlots() {
            int c = 0;
            for (int i = 0; i < threads_array.Length; i++) {
                if (threads_array[i] == null || !threads_array[i].IsAlive) c++;
            }
            return c;
        }

        private void abortAllThreads() {
            for (int i = 0; i < threads_array.Length; i++) {
                if (threads_array[i] == null) continue;
                if (threads_array[i].IsAlive) {
                    threads_array[i].Abort();
                    threads_array[i].Join();
                }
            }
        }

        // ===================================== ThreadSafe static counter methods ============================
        internal static void incrementNotFoundTrackCount() {
            lock (counterLocker) {
                lyrics_not_found++;
            }
        }
        internal static void incrementErrorsTrackCount() {
            lock (counterLocker) {
                lyrics_errors++;
            }
        }
        internal static int getNotFoundTrackCount() {
            return lyrics_not_found;
        }
        internal static int getErrorsTrackCount() {
            return lyrics_errors;
        }
        internal static int getTotalErrors() {
            return lyrics_errors + lyrics_not_found;
        }
        internal static int getProcessedTracks() {
            return current_track_index;
        }


    }
}
