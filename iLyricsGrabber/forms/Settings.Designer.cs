﻿namespace iLyricsGrabber
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.richTextBox6 = new System.Windows.Forms.RichTextBox();
            this.udpates_checkbox = new System.Windows.Forms.CheckBox();
            this.privacypolicy_linklabel = new System.Windows.Forms.LinkLabel();
            this.richTextBox5 = new System.Windows.Forms.RichTextBox();
            this.usage_checkbox = new System.Windows.Forms.CheckBox();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.logging_checkbox = new System.Windows.Forms.CheckBox();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.overwrite_checkbox = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.parallelworks_numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timeout_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.apply_button = new System.Windows.Forms.Button();
            this.ok_button = new System.Windows.Forms.Button();
            this.cancel_button = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.parallelworks_numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeout_numericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(401, 253);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.richTextBox6);
            this.tabPage1.Controls.Add(this.udpates_checkbox);
            this.tabPage1.Controls.Add(this.privacypolicy_linklabel);
            this.tabPage1.Controls.Add(this.richTextBox5);
            this.tabPage1.Controls.Add(this.usage_checkbox);
            this.tabPage1.Controls.Add(this.richTextBox4);
            this.tabPage1.Controls.Add(this.logging_checkbox);
            this.tabPage1.Controls.Add(this.richTextBox3);
            this.tabPage1.Controls.Add(this.overwrite_checkbox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(393, 227);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // richTextBox6
            // 
            this.richTextBox6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.richTextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox6.Cursor = System.Windows.Forms.Cursors.Default;
            this.richTextBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.richTextBox6.Location = new System.Drawing.Point(25, 203);
            this.richTextBox6.Name = "richTextBox6";
            this.richTextBox6.ReadOnly = true;
            this.richTextBox6.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBox6.Size = new System.Drawing.Size(359, 14);
            this.richTextBox6.TabIndex = 11;
            this.richTextBox6.Text = "If this option is enabled, the application will check for updates automatically a" +
    "t startup\n";
            // 
            // udpates_checkbox
            // 
            this.udpates_checkbox.AutoSize = true;
            this.udpates_checkbox.Location = new System.Drawing.Point(6, 183);
            this.udpates_checkbox.Name = "udpates_checkbox";
            this.udpates_checkbox.Size = new System.Drawing.Size(162, 17);
            this.udpates_checkbox.TabIndex = 10;
            this.udpates_checkbox.Text = "Check updates automatically";
            this.udpates_checkbox.UseVisualStyleBackColor = true;
            // 
            // privacypolicy_linklabel
            // 
            this.privacypolicy_linklabel.AutoSize = true;
            this.privacypolicy_linklabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.privacypolicy_linklabel.Location = new System.Drawing.Point(320, 126);
            this.privacypolicy_linklabel.Name = "privacypolicy_linklabel";
            this.privacypolicy_linklabel.Size = new System.Drawing.Size(64, 12);
            this.privacypolicy_linklabel.TabIndex = 9;
            this.privacypolicy_linklabel.TabStop = true;
            this.privacypolicy_linklabel.Text = "Privacy Policy";
            this.privacypolicy_linklabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Privacypolicy_linklabel_LinkClicked);
            // 
            // richTextBox5
            // 
            this.richTextBox5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.richTextBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox5.Cursor = System.Windows.Forms.Cursors.Default;
            this.richTextBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.richTextBox5.Location = new System.Drawing.Point(25, 144);
            this.richTextBox5.Name = "richTextBox5";
            this.richTextBox5.ReadOnly = true;
            this.richTextBox5.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBox5.Size = new System.Drawing.Size(359, 33);
            this.richTextBox5.TabIndex = 8;
            this.richTextBox5.Text = "Enable this option to send anonymous advanced usage data to developer in order to" +
    " improve the application experience";
            // 
            // usage_checkbox
            // 
            this.usage_checkbox.AutoSize = true;
            this.usage_checkbox.Location = new System.Drawing.Point(6, 124);
            this.usage_checkbox.Name = "usage_checkbox";
            this.usage_checkbox.Size = new System.Drawing.Size(164, 17);
            this.usage_checkbox.TabIndex = 7;
            this.usage_checkbox.Text = "Send anonymous usage data";
            this.usage_checkbox.UseVisualStyleBackColor = true;
            // 
            // richTextBox4
            // 
            this.richTextBox4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.richTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox4.Cursor = System.Windows.Forms.Cursors.Default;
            this.richTextBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.richTextBox4.Location = new System.Drawing.Point(25, 77);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.ReadOnly = true;
            this.richTextBox4.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBox4.Size = new System.Drawing.Size(359, 41);
            this.richTextBox4.TabIndex = 6;
            this.richTextBox4.Text = resources.GetString("richTextBox4.Text");
            // 
            // logging_checkbox
            // 
            this.logging_checkbox.AutoSize = true;
            this.logging_checkbox.Location = new System.Drawing.Point(6, 58);
            this.logging_checkbox.Name = "logging_checkbox";
            this.logging_checkbox.Size = new System.Drawing.Size(64, 17);
            this.logging_checkbox.TabIndex = 5;
            this.logging_checkbox.Text = "Logging";
            this.logging_checkbox.UseVisualStyleBackColor = true;
            // 
            // richTextBox3
            // 
            this.richTextBox3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.richTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox3.Cursor = System.Windows.Forms.Cursors.Default;
            this.richTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.richTextBox3.Location = new System.Drawing.Point(25, 24);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.ReadOnly = true;
            this.richTextBox3.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBox3.Size = new System.Drawing.Size(359, 28);
            this.richTextBox3.TabIndex = 4;
            this.richTextBox3.Text = "Choose if overwrite existing lyrics that are already present in tracks. If select" +
    "ed they will be skipped";
            // 
            // overwrite_checkbox
            // 
            this.overwrite_checkbox.AutoSize = true;
            this.overwrite_checkbox.Location = new System.Drawing.Point(6, 6);
            this.overwrite_checkbox.Name = "overwrite_checkbox";
            this.overwrite_checkbox.Size = new System.Drawing.Size(135, 17);
            this.overwrite_checkbox.TabIndex = 0;
            this.overwrite_checkbox.Text = "Overwrite existing lyrics";
            this.overwrite_checkbox.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.richTextBox2);
            this.tabPage2.Controls.Add(this.parallelworks_numericUpDown1);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.richTextBox1);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.timeout_numericUpDown);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(393, 227);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Lyrics Searching";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // richTextBox2
            // 
            this.richTextBox2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox2.Cursor = System.Windows.Forms.Cursors.Default;
            this.richTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.richTextBox2.Location = new System.Drawing.Point(9, 101);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBox2.Size = new System.Drawing.Size(378, 42);
            this.richTextBox2.TabIndex = 6;
            this.richTextBox2.Text = "Set the number of parallel lyrics searching works when clicked on \"Grab Lyrics\". " +
    "Default and suggested is 5 parallel work but you can increase it (if your PC and" +
    " your connection are fast) until 20.";
            // 
            // parallelworks_numericUpDown1
            // 
            this.parallelworks_numericUpDown1.Location = new System.Drawing.Point(317, 75);
            this.parallelworks_numericUpDown1.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.parallelworks_numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.parallelworks_numericUpDown1.Name = "parallelworks_numericUpDown1";
            this.parallelworks_numericUpDown1.Size = new System.Drawing.Size(70, 20);
            this.parallelworks_numericUpDown1.TabIndex = 5;
            this.parallelworks_numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Max parallel lyrics searching";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.richTextBox1.Location = new System.Drawing.Point(9, 32);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBox1.Size = new System.Drawing.Size(378, 42);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Maximum time to wait when downloading lyrics (ms)";
            // 
            // timeout_numericUpDown
            // 
            this.timeout_numericUpDown.Location = new System.Drawing.Point(317, 6);
            this.timeout_numericUpDown.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.timeout_numericUpDown.Minimum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.timeout_numericUpDown.Name = "timeout_numericUpDown";
            this.timeout_numericUpDown.Size = new System.Drawing.Size(70, 20);
            this.timeout_numericUpDown.TabIndex = 1;
            this.timeout_numericUpDown.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // apply_button
            // 
            this.apply_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.apply_button.Location = new System.Drawing.Point(176, 271);
            this.apply_button.Name = "apply_button";
            this.apply_button.Size = new System.Drawing.Size(75, 23);
            this.apply_button.TabIndex = 2;
            this.apply_button.Text = "&Apply";
            this.apply_button.UseVisualStyleBackColor = true;
            this.apply_button.Click += new System.EventHandler(this.apply_button_Click);
            // 
            // ok_button
            // 
            this.ok_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ok_button.Location = new System.Drawing.Point(257, 271);
            this.ok_button.Name = "ok_button";
            this.ok_button.Size = new System.Drawing.Size(75, 23);
            this.ok_button.TabIndex = 3;
            this.ok_button.Text = "&OK";
            this.ok_button.UseVisualStyleBackColor = true;
            this.ok_button.Click += new System.EventHandler(this.ok_button_Click);
            // 
            // cancel_button
            // 
            this.cancel_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancel_button.Location = new System.Drawing.Point(338, 271);
            this.cancel_button.Name = "cancel_button";
            this.cancel_button.Size = new System.Drawing.Size(75, 23);
            this.cancel_button.TabIndex = 4;
            this.cancel_button.Text = "&Cancel";
            this.cancel_button.UseVisualStyleBackColor = true;
            this.cancel_button.Click += new System.EventHandler(this.cancel_button_Click);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 306);
            this.Controls.Add(this.cancel_button);
            this.Controls.Add(this.ok_button);
            this.Controls.Add(this.apply_button);
            this.Controls.Add(this.tabControl1);
            this.Name = "Settings";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.Settings_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.parallelworks_numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeout_numericUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckBox overwrite_checkbox;
        private System.Windows.Forms.Button apply_button;
        private System.Windows.Forms.Button ok_button;
        private System.Windows.Forms.Button cancel_button;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown timeout_numericUpDown;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.NumericUpDown parallelworks_numericUpDown1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.CheckBox logging_checkbox;
        private System.Windows.Forms.RichTextBox richTextBox5;
        private System.Windows.Forms.CheckBox usage_checkbox;
        private System.Windows.Forms.LinkLabel privacypolicy_linklabel;
        private System.Windows.Forms.RichTextBox richTextBox6;
        private System.Windows.Forms.CheckBox udpates_checkbox;
    }
}