﻿namespace iLyricsGrabber {
    partial class ID3TagForcer {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.info_label = new System.Windows.Forms.Label();
            this.start_button = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.attention_label = new System.Windows.Forms.Label();
            this.status_label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // info_label
            // 
            this.info_label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.info_label.Location = new System.Drawing.Point(12, 9);
            this.info_label.Name = "info_label";
            this.info_label.Size = new System.Drawing.Size(393, 30);
            this.info_label.TabIndex = 0;
            this.info_label.Text = "This tool will force iTunes to write the ID3 tag to the track file. This is usefu" +
    "l if you use your library with other software or just if you want to keep it org" +
    "anized.";
            // 
            // start_button
            // 
            this.start_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.start_button.Location = new System.Drawing.Point(330, 57);
            this.start_button.Name = "start_button";
            this.start_button.Size = new System.Drawing.Size(75, 23);
            this.start_button.TabIndex = 1;
            this.start_button.Text = "&Start";
            this.start_button.UseVisualStyleBackColor = true;
            this.start_button.Click += new System.EventHandler(this.start_button_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(15, 57);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(311, 23);
            this.progressBar1.TabIndex = 2;
            // 
            // attention_label
            // 
            this.attention_label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.attention_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attention_label.ForeColor = System.Drawing.Color.Red;
            this.attention_label.Location = new System.Drawing.Point(12, 85);
            this.attention_label.Name = "attention_label";
            this.attention_label.Size = new System.Drawing.Size(391, 13);
            this.attention_label.TabIndex = 3;
            this.attention_label.Text = "Please use this tool with a few tracks! It deals with metadata. Don\'t use iTunes " +
    "while working!";
            this.attention_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // status_label
            // 
            this.status_label.AutoSize = true;
            this.status_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.status_label.Location = new System.Drawing.Point(13, 42);
            this.status_label.Name = "status_label";
            this.status_label.Size = new System.Drawing.Size(54, 12);
            this.status_label.TabIndex = 4;
            this.status_label.Text = "StatusLabel";
            // 
            // ID3TagForcer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 107);
            this.Controls.Add(this.status_label);
            this.Controls.Add(this.attention_label);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.start_button);
            this.Controls.Add(this.info_label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ID3TagForcer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ID3TagForcer";
            this.Load += new System.EventHandler(this.ID3TagForcer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label info_label;
        private System.Windows.Forms.Button start_button;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label attention_label;
        private System.Windows.Forms.Label status_label;
    }
}