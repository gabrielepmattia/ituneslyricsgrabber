﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace iLyricsGrabber {
    public partial class Settings : Form {

        private const string LINK_PRIVACY_POLICY = "https://itlg.gabrielepmattia.com/privacy_policy";

        public Settings() {
            InitializeComponent();
        }

        private void Settings_Load(object sender, EventArgs e) {
            overwrite_checkbox.Checked = Properties.Settings.Default.Overwrite;
            logging_checkbox.Checked = Properties.Settings.Default.Log;
            timeout_numericUpDown.Value = Properties.Settings.Default.WebRequestTimeOut;
            parallelworks_numericUpDown1.Value = Properties.Settings.Default.MaxParallelWorks;
            usage_checkbox.Checked = Properties.Settings.Default.SettingAnalytics;
            udpates_checkbox.Checked = Properties.Settings.Default.SettingAutoUpdates;
        }

        private void apply_button_Click(object sender, EventArgs e) {
            Properties.Settings.Default.Overwrite = overwrite_checkbox.Checked;
            Properties.Settings.Default.Log = logging_checkbox.Checked;
            Properties.Settings.Default.WebRequestTimeOut = (int)timeout_numericUpDown.Value;
            Properties.Settings.Default.MaxParallelWorks = (int)parallelworks_numericUpDown1.Value;
            Properties.Settings.Default.SettingAnalytics = usage_checkbox.Checked;
            Properties.Settings.Default.SettingAutoUpdates = udpates_checkbox.Checked;
            Properties.Settings.Default.Save();
        }

        private void ok_button_Click(object sender, EventArgs e) {
            apply_button_Click(null, null);
            this.Close();
        }

        private void cancel_button_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Privacypolicy_linklabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            Process.Start(LINK_PRIVACY_POLICY);
        }
    }
}
