﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using iTunesLib;

namespace iLyricsGrabber {
    public partial class TracksCount : Form {
        // Create the itunes object
        iTunesLib.iTunesApp oItunes = new iTunesLib.iTunesApp();
        public TracksCount() {
            InitializeComponent();
        }

        private void count_button_Click(object sender, EventArgs e) {
            AlbumName_radio.Enabled = false;
            AlbumPic_radio.Enabled = false;
            Lyrics_radio.Enabled = false;

            label2.Text = "Obtaining selected tracks..";
            label2.Visible = true;
            try {
                // Crea oggetto "selected_tracks" per le tracce selezionate
                iTunesLib.IITTrackCollection selected_tracks = oItunes.SelectedTracks;
                // Crea variabile intera "selected_tracks_count" con il numero delle tracce selezionate
                int selected_tracks_count = selected_tracks.Count;
                // Setto la progress bar
                progressBar1.Enabled = true;
                progressBar1.Maximum = selected_tracks_count;
                progressBar1.Step = 1;
                progressBar1.Refresh();
                // Azzero i contatori
                int counter = 0;
                int counted = 0;
                // Attivo la tabella
                dataGridView1.Enabled = true;
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                // Creo il ciclo di analisi delle tracce
                foreach (IITFileOrCDTrack track in selected_tracks) {
                    // Incremento il contatore
                    counter++;
                    label2.Text = counter + " of " + selected_tracks_count;
                    progressBar1.PerformStep();
                    // Setto le variabili della traccia
                    string Artist = track.Artist;
                    string Title = track.Name;
                    string Album = track.Album;
                    if (AlbumPic_radio.Checked == true) {
                        if (track.Artwork.Count == 0) {
                            counted++;
                            // Creo la riga della tabella con le info
                            string[] row = new string[] { counted.ToString(), Title, Artist, Album };
                            dataGridView1.Rows.Add(row);
                        }
                    }

                    if (Lyrics_radio.Checked == true) {
                        if (track.Lyrics == null) {
                            counted++;
                            // Creo la riga della tabella con le info
                            string[] row = new string[] { counted.ToString(), Title, Artist, Album };
                            dataGridView1.Rows.Add(row);
                        }
                    }

                    if (AlbumName_radio.Checked == true) {
                        if (track.Album == null) {
                            counted++;
                            // Creo la riga della tabella con le info
                            string[] row = new string[] { counted.ToString(), Title, Artist, "//" };
                            dataGridView1.Rows.Add(row);
                        }
                    }
                } // Fine del foreach

                label2.Text = counted + " track(s) counted.";
                AlbumName_radio.Enabled = true;
                AlbumPic_radio.Enabled = true;
                Lyrics_radio.Enabled = true;
            } catch (NullReferenceException)// gli errori nella creazione dell'oggetto iTunes
              {
                MessageBox.Show("Make sure you have selected at least 1 item and iTunes doesn't require your attention to go on!", "Error accessing selected tracks", MessageBoxButtons.OK, MessageBoxIcon.Error);
                label2.Text = "Ready!";
            }
        }


        private void TracksCount_Load(object sender, EventArgs e) {

        }
    }
}
