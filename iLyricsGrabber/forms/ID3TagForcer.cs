﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using iTunesLib;

namespace iLyricsGrabber {
    public partial class ID3TagForcer : Form {
        // Create the itunes object
        iTunesLib.iTunesApp oItunes = new iTunesLib.iTunesApp();
        // Globals
        int total_selected_tracks;
        int current_track_index;

        public ID3TagForcer() {
            InitializeComponent();
        }

        private void start_button_Click(object sender, EventArgs e) {
            iTunesLib.IITTrackCollection selected_tracks = oItunes.SelectedTracks;

            current_track_index = 0;
            total_selected_tracks = selected_tracks.Count;

            prepareView();
            foreach (IITFileOrCDTrack track in selected_tracks) {
                // Incremento il contatore
                current_track_index++;
                progressBar1.PerformStep();

                status_label.Text = "Track " + current_track_index + " of " + total_selected_tracks;
                status_label.Refresh();

                // Force id3
                if (track.Artist != null) {
                    String original_artist = track.Artist;
                    track.Artist = "<TempArtist>";
                    track.Artist = original_artist;
                }

                if(track.Name != null) {
                    String original_name = track.Name;
                    track.Name = "<TempName>";
                    track.Name = original_name;
                }
                
                if(track.Album != null) {
                    String original_album = track.Album;
                    track.Album = "<TempAlbum>";
                    track.Album = original_album;
                }
                
                if(track.AlbumArtist != null) {
                    String original_albumartist = track.AlbumArtist;
                    track.AlbumArtist = "<TempAlbumArtist>";
                    track.AlbumArtist = original_albumartist;
                }
            
                if(track.Composer != null) {
                    String original_composer = track.Composer;
                    track.Composer = "<TempComposer>";
                    track.Composer = original_composer;
                }

                if (track.Genre != null) {
                    String original_genre = track.Genre;
                    track.Genre = "<TempGenre>";
                    track.Genre = original_genre;
                }

                if (track.TrackNumber != 0) {
                    int original_track_number = track.TrackNumber;
                    track.TrackNumber = 1;
                    track.TrackNumber = original_track_number;
                }


            }
        }

        private void prepareView() {
            progressBar1.Refresh();
            progressBar1.Maximum = total_selected_tracks;
            progressBar1.Step = 1;
        }

        private void ID3TagForcer_Load(object sender, EventArgs e) {
            status_label.Text = "Ready! Select track on iTunes and click Start";
        }
    }
}
