﻿namespace iLyricsGrabber
{
    partial class PlayCountSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.selectedtracks_group = new System.Windows.Forms.GroupBox();
            this.played_selected = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.title_selected = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.custompc_textbox = new System.Windows.Forms.TextBox();
            this.custom_groupbox = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.result = new System.Windows.Forms.Label();
            this.selectedtracks_group.SuspendLayout();
            this.custom_groupbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(201, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Use this tool to set Played Count number.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(441, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "1. Go on iTunes and select one (1) track you want to change Played Count and clic" +
    "k Check";
            // 
            // selectedtracks_group
            // 
            this.selectedtracks_group.Controls.Add(this.played_selected);
            this.selectedtracks_group.Controls.Add(this.label4);
            this.selectedtracks_group.Controls.Add(this.title_selected);
            this.selectedtracks_group.Controls.Add(this.label3);
            this.selectedtracks_group.Location = new System.Drawing.Point(34, 50);
            this.selectedtracks_group.Name = "selectedtracks_group";
            this.selectedtracks_group.Size = new System.Drawing.Size(300, 71);
            this.selectedtracks_group.TabIndex = 2;
            this.selectedtracks_group.TabStop = false;
            this.selectedtracks_group.Text = "Selected Track";
            // 
            // played_selected
            // 
            this.played_selected.Location = new System.Drawing.Point(207, 32);
            this.played_selected.Name = "played_selected";
            this.played_selected.ReadOnly = true;
            this.played_selected.Size = new System.Drawing.Size(69, 20);
            this.played_selected.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(204, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Played count:";
            // 
            // title_selected
            // 
            this.title_selected.Location = new System.Drawing.Point(23, 32);
            this.title_selected.Name = "title_selected";
            this.title_selected.ReadOnly = true;
            this.title_selected.Size = new System.Drawing.Size(156, 20);
            this.title_selected.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Title:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(358, 74);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(72, 35);
            this.button1.TabIndex = 3;
            this.button1.Text = "Check!";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(146, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "2. Now type desidered count:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Custom played count:";
            // 
            // custompc_textbox
            // 
            this.custompc_textbox.Location = new System.Drawing.Point(23, 34);
            this.custompc_textbox.Name = "custompc_textbox";
            this.custompc_textbox.Size = new System.Drawing.Size(69, 20);
            this.custompc_textbox.TabIndex = 6;
            // 
            // custom_groupbox
            // 
            this.custom_groupbox.Controls.Add(this.button2);
            this.custom_groupbox.Controls.Add(this.custompc_textbox);
            this.custom_groupbox.Controls.Add(this.label6);
            this.custom_groupbox.Enabled = false;
            this.custom_groupbox.Location = new System.Drawing.Point(34, 146);
            this.custom_groupbox.Name = "custom_groupbox";
            this.custom_groupbox.Size = new System.Drawing.Size(300, 72);
            this.custom_groupbox.TabIndex = 7;
            this.custom_groupbox.TabStop = false;
            this.custom_groupbox.Text = "Custom settings";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(201, 25);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 36);
            this.button2.TabIndex = 7;
            this.button2.Text = "Set";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // result
            // 
            this.result.AutoSize = true;
            this.result.Location = new System.Drawing.Point(376, 183);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(41, 13);
            this.result.TabIndex = 8;
            this.result.Text = "Ready!";
            // 
            // PlayCountSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 234);
            this.Controls.Add(this.result);
            this.Controls.Add(this.custom_groupbox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.selectedtracks_group);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PlayCountSetting";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Play Count Setting Tool";
            this.selectedtracks_group.ResumeLayout(false);
            this.selectedtracks_group.PerformLayout();
            this.custom_groupbox.ResumeLayout(false);
            this.custom_groupbox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox selectedtracks_group;
        private System.Windows.Forms.TextBox played_selected;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox title_selected;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox custompc_textbox;
        private System.Windows.Forms.GroupBox custom_groupbox;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label result;
    }
}