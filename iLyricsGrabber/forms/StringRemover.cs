﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using iTunesLib;

namespace iLyricsGrabber {
    public partial class StringRemover : Form {
        //Create the itunes object
        iTunesLib.iTunesApp oItunes = new iTunesLib.iTunesApp();

        public StringRemover() {
            InitializeComponent();
        }

        private void remove_button_Click(object sender, EventArgs e) {
            // Crea oggetto "selected_tracks" per le tracce selezionate
            StatusLabel1.Text = "Obtaining selected tracks..";
            iTunesLib.IITTrackCollection selected_tracks = oItunes.SelectedTracks;

            // Crea variabile intera "selected_tracks_count" con il numero delle tracce selezionate
            int selected_tracks_count = selected_tracks.Count;

            // Setto la progress bar
            progressBar1.Refresh();
            progressBar1.Enabled = true;
            progressBar1.Maximum = selected_tracks_count;
            progressBar1.Step = 1;

            string removestr = StringToRemove_textbox.Text;

            // Azzero contatori
            int counter = 0;
            label4.Visible = true;
            label4.Text = "";

            foreach (IITFileOrCDTrack track in selected_tracks) {
                // Incremento il contatore
                counter++;
                label4.Text = counter + " of " + selected_tracks_count;

                progressBar1.PerformStep();
                try {
                    if (checkedListBox1.GetItemChecked(0) == true) // Title Checked
                    {
                        string Title = track.Name;
                        StatusLabel1.Text = "Checking title";
                        string strname = "";
                        strname = Title.Replace(removestr, "");
                        track.Name = strname;
                    }

                    if (checkedListBox1.GetItemChecked(1) == true) // Title Checked
                    {
                        string Artist = track.Artist;
                        StatusLabel1.Text = "Checking artist";
                        string strArtist = "";
                        strArtist = Artist.Replace(removestr, "");
                        track.Artist = strArtist;
                    }

                    if (checkedListBox1.GetItemChecked(2) == true) // Title Checked
                    {
                        string Album = track.Album;
                        StatusLabel1.Text = "Checking album";
                        string strAlbum = "";
                        strAlbum = Album.Replace(removestr, "");
                        track.Album = strAlbum;
                    }

                    if (checkedListBox1.GetItemChecked(3) == true) // Title Checked
                    {
                        string AArtist = track.AlbumArtist;
                        StatusLabel1.Text = "Checking Album Artist";
                        string strAArtist = "";
                        strAArtist = AArtist.Replace(removestr, "");
                        track.AlbumArtist = strAArtist;
                    }

                    if (checkedListBox1.GetItemChecked(4) == true) // Title Checked
                    {
                        string Group = track.Grouping;
                        StatusLabel1.Text = "Checking Group";
                        string strGroup = "";
                        strGroup = Group.Replace(removestr, "");
                        track.Grouping = strGroup;
                    }

                    if (checkedListBox1.GetItemChecked(5) == true) // Title Checked
                    {
                        string Composer = track.Composer;
                        StatusLabel1.Text = "Checking Composer";
                        string strComposer = "";
                        strComposer = Composer.Replace(removestr, "");
                        track.Composer = strComposer;
                    }

                    if (checkedListBox1.GetItemChecked(6) == true) // Title Checked
                    {
                        string Comment = track.Comment;
                        StatusLabel1.Text = "Checking Comment";
                        string strComment = "";
                        strComment = Comment.Replace(removestr, "");
                        track.Comment = strComment;
                    }

                    if (checkedListBox1.GetItemChecked(7) == true) // Title Checked
                    {
                        string Descr = track.Description;
                        StatusLabel1.Text = "Checking Description (Video)";
                        string strDescr = "";
                        strDescr = Descr.Replace(removestr, "");
                        track.Description = strDescr;
                    }

                    if (checkedListBox1.GetItemChecked(8) == true) // Title Checked
                    {
                        string Lyrics = track.Lyrics;
                        StatusLabel1.Text = "Checking Lyrics";
                        string strLyrics = "";
                        strLyrics = Lyrics.Replace(removestr, "");
                        track.Lyrics = strLyrics;
                    }

                    if (checkedListBox1.GetItemChecked(9) == true) // Title Checked
                    {
                        string Show = track.Show;
                        StatusLabel1.Text = "Checking Show";
                        string strShow = "";
                        strShow = Show.Replace(removestr, "");
                        track.Show = strShow;
                    }

                    if (checkedListBox1.GetItemChecked(10) == true) // Title Checked
                    {
                        string SName = track.SortName;
                        StatusLabel1.Text = "Checking Sort Name";
                        string strSName = "";
                        strSName = SName.Replace(removestr, "");
                        track.SortName = strSName;
                    }

                    if (checkedListBox1.GetItemChecked(11) == true) // Title Checked
                    {
                        string SAlbum = track.SortAlbum;
                        StatusLabel1.Text = "Checking Sort Album";
                        string strSAlbum = "";
                        strSAlbum = SAlbum.Replace(removestr, "");
                        track.SortAlbum = strSAlbum;
                    }

                    if (checkedListBox1.GetItemChecked(12) == true) // Title Checked
                    {
                        string SArtist = track.SortArtist;
                        StatusLabel1.Text = "Checking Sort Artist";
                        string strSArtist = "";
                        strSArtist = SArtist.Replace(removestr, "");
                        track.SortArtist = strSArtist;
                    }

                    if (checkedListBox1.GetItemChecked(13) == true) // Title Checked
                    {
                        string SAArtist = track.SortAlbumArtist;
                        StatusLabel1.Text = "Checking Sort Album Artist";
                        string strSAArtist = "";
                        strSAArtist = SAArtist.Replace(removestr, "");
                        track.SortAlbumArtist = strSAArtist;
                    }

                    if (checkedListBox1.GetItemChecked(14) == true) // Title Checked
                    {
                        string SComposer = track.SortComposer;
                        StatusLabel1.Text = "Checking Sort Composer";
                        string strSComposer = "";
                        strSComposer = SComposer.Replace(removestr, "");
                        track.SortComposer = strSComposer;
                    }

                    if (checkedListBox1.GetItemChecked(15) == true) // Title Checked
                    {
                        string SShow = track.SortShow;
                        StatusLabel1.Text = "Checking Sort Show";
                        string strSShow = "";
                        strSShow = SShow.Replace(removestr, "");
                        track.SortShow = strSShow;
                    }

                } catch (NullReferenceException) {
                    StatusLabel1.Text = "Empty Data";

                }
            }

            label4.Visible = false;
            StatusLabel1.Text = "Done!";

        }

        private void button1_Click(object sender, EventArgs e) {
            checkedListBox1.SetItemChecked(0, true);
            checkedListBox1.SetItemChecked(1, true);
            checkedListBox1.SetItemChecked(2, true);
            checkedListBox1.SetItemChecked(3, true);
            checkedListBox1.SetItemChecked(4, true);
            checkedListBox1.SetItemChecked(5, true);
            checkedListBox1.SetItemChecked(6, true);
            checkedListBox1.SetItemChecked(7, true);
            checkedListBox1.SetItemChecked(8, true);
            checkedListBox1.SetItemChecked(9, true);
            checkedListBox1.SetItemChecked(10, true);
            checkedListBox1.SetItemChecked(11, true);
            checkedListBox1.SetItemChecked(12, true);
            checkedListBox1.SetItemChecked(13, true);
            checkedListBox1.SetItemChecked(14, true);
            checkedListBox1.SetItemChecked(15, true);
        }

        private void StringRemover_Load(object sender, EventArgs e) {

        }
    }
}
