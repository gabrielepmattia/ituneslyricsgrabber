﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Management;
using System.Threading;
using System.Diagnostics;
using System.Threading.Tasks;

namespace iLyricsGrabber {
    public partial class Starting : Form {
        // Delegate for cross thread call to close
        private delegate void CloseDelegate();

        // The type of form to be displayed as the splash screen.
        private static Starting starting;

        public Starting() {
            InitializeComponent();
            Application.EnableVisualStyles();
            progressBar1.Enabled = true;
            progressBar1.Style = ProgressBarStyle.Marquee;
            progressBar1.MarqueeAnimationSpeed = 30;
        }

        static public void ShowSplashScreen() {
            // Make sure it is only launched once.
            if (starting != null) return;
            Thread thread = new Thread(new ThreadStart(Starting.ShowForm));
            thread.IsBackground = true;
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        static private void ShowForm() {
            starting = new Starting();
            Application.Run(starting);
        }

        static public void CloseForm() {
            starting.Invoke(new CloseDelegate(Starting.CloseFormInternal));
        }

        static private void CloseFormInternal() {
            starting.Close();
        }

        private void Starting_Load(object sender, EventArgs e) {
            label3.Text = "v." + Application.ProductVersion;
            label2.Text = "Loading components..";
        }

        /*
         void check_iTunes(object sender, EventArgs e) {
            timer1.Stop();
            Process[] localByName = Process.GetProcessesByName("iTunes"); // Controlla se iTunes è avviato
            if (localByName.Length == 0) {
                //set time interval 3 sec
                timer1.Interval = 2000;
                //starts the timer
                timer1.Start();
                timer1.Tick += check_iTunes;

            } else {
                StartApp();
            }

        }
       

        private void StartApp() {
            this.Hide();
            Main main = new Main();
            main.ShowDialog();
        }
         */

        private void richTextBox1_Enter(object sender, EventArgs e) {
            ActiveControl = null;
        }
    }
}
