﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace iLyricsGrabber {
    public partial class Instructions : Form {
        public Instructions() {
            InitializeComponent();
        }

        private void go_button_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void richTextBox1_Enter(object sender, EventArgs e) {
            ActiveControl = null;
        }

        private void Instructions_Load(object sender, EventArgs e) {
            version_label.Text = "Brought to you by gabry3795, your version is v" + Application.ProductVersion;
            usage_checkbox.Checked = Properties.Settings.Default.SettingAnalytics;
            usage_checkbox.CheckedChanged += UsageCheckBoxStateChanged;
        }

        /*
         * Listeners
         */

        private void UsageCheckBoxStateChanged(object sender, EventArgs e) {
            Properties.Settings.Default.SettingAnalytics = usage_checkbox.Checked;
            Properties.Settings.Default.Save();
        }
    }
}
