﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using iTunesLib;
using System.Threading;
using System.ComponentModel;

using iLyricsGrabber.workers;
using iLyricsGrabber.ui;

namespace iLyricsGrabber {

    public partial class Main : Form {
        public const int FETCHING_STARTED = 1000;
        public const int STATUS_SEEKING = 2000;
        public const int STATUS_LYRICS_FOUND = 2001;
        public const int STATUS_LYRICS_NOT_FOUND = 2002;
        public const int STATUS_COMPLETED = 2003;
        public const int STATUS_TRACK_SKIPPED = 2004;
        public const int STATUS_TIMEOUT = 2005;
        public const int STATUS_WEBERROR = 2006;
        public const int STATUS_COMERROR = 2007;
        public const int STATUS_TRACK_ERROR = 2008;
        public const int STATUS_SENDING_LYRICS_ERROR = 2009;
        public const int STATUS_ABORTED = 2010;
        public static String LOGFILE_NAME = System.IO.Path.GetTempPath() + "ituneslyricsgrabber_log.txt";

        //Create the itunes object
        iTunesLib.iTunesApp oItunes = new iTunesLib.iTunesApp();

        public int TA = Properties.Settings.Default.TA;
        public int MTA = Properties.Settings.Default.MTA;
        public int SN = Properties.Settings.Default.SN;
        public bool ovw = Properties.Settings.Default.Overwrite;
        //public string init_html = "<html><body bgcolor=\"#0178bc\" style=\"font-family: Verdana, Geneva, sans-serif;font-size:10px;color:#FFF;\"><p style=\"text-align:center\"><br />Welcome to <b>iTunesLyricsGrabber</b>! ;)<br /><br />This software will add lyrics to your iTunes tracks.<br /><br /> All you have to do is select tracks in iTunes and come back here to click \"Grab Lyrics\" to go on. You can press abort button in any moment.</p><br /><p>Please remember that: <br />- You can choose to overwrite lyrics that are already in your tracks, just click on \"Overwrite\" button (Green = enabled, Red = disabled)<br />- Artist name and album <u><b>MUST be</b></u> correct and not misspelled!<br />- You can't use your pc while app is working<br />- You must be connected to the internet<br />- Under the top menu \"Tools\" you can find some useful tools for organizing you library (string remover, tracks counter, ..)</p><p style=\"text-align:center\">Thank you for using this app!</p></body></html>";
        Thread lyricsFetcherWorkerThread;
        bool in_progress = false;
        bool logged_start_message = false;
        // Lockers
        private Object logLocker = new Object();
        private Object tableLocker = new object();

        public Main() {
            InitializeComponent();
            Properties.Settings.Default.SN++;
            Properties.Settings.Default.Save();
            dataGridView1.RowsAdded += new DataGridViewRowsAddedEventHandler(dataGridView1_RowsAdded);
            logStartMessage();
        }

        /// <summary>
        /// Update and sort the table when row is added
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e) {
            dataGridView1.Refresh();
            this.dataGridView1.Sort(this.dataGridView1.Columns[0], ListSortDirection.Ascending);
            dataGridView1.FirstDisplayedScrollingRowIndex = dataGridView1.Rows.Count == 0 ? dataGridView1.Rows.Count : dataGridView1.Rows.Count - 1;
            dataGridView1.ClearSelection();

            ThreadSafeMethods.threadSafeUpdateProgessBar(progressBar1, dataGridView1.Rows.Count);
            ThreadSafeMethods.threadSafeLabelUpdate(processed_tracks_value, dataGridView1.Rows.Count.ToString());
            ThreadSafeMethods.threadSafeLabelUpdate(remaining_tracks_value, (Int32.Parse(remaining_tracks_value.Text) - 1).ToString());
        }

        public void Log(string logMessage) {
            if (!Properties.Settings.Default.Log) return;
            if (!logged_start_message) {
                logStartMessage();
                logged_start_message = true;
            }
            lock (logLocker) {
                StreamWriter w = File.AppendText(LOGFILE_NAME);
                w.WriteLine("{0} {1}::: {2}", DateTime.Now.ToShortTimeString(), DateTime.Now.ToShortDateString(), logMessage);
                w.Dispose();
            }
        }

        private void logStartMessage() {
            if (!Properties.Settings.Default.Log) return;
            lock (logLocker) {
                DotNetWindowsInfo.models.WindowsInfo winfo = DotNetWindowsInfo.SystemInfo.GetInfo();
                StreamWriter w = File.AppendText(LOGFILE_NAME);
                w.WriteLine("***");
                w.WriteLine("");
                w.WriteLine("===== iTunesLyricsGrabber v{0} Log File =====", Application.ProductVersion);
                w.WriteLine(".NET Framework {0} - {1}", utils.DotNet.GetVersion(), winfo != null ? winfo.ToString() : "");
                w.WriteLine("If you have problems see itlg.gabrielepmattia.com/bugs/. Don't modify this file");
                w.WriteLine("TA={0}, MTA={1}, ovw={2}, SN={3} ", TA.ToString(), MTA.ToString(), ovw.ToString(), SN.ToString());
                w.WriteLine("Started session");
                w.WriteLine("");
                w.Dispose();
            }
        }

        private void grab_button_Click(object sender, EventArgs e) {
            utils.Analytics.GetInstance().GrabPressed();
            Log("-- MainWorker::ButtonPressed::Pressed Grab button");
            if (in_progress) { Abort_Click(null, null); return; }
            if (oItunes.SelectedTracks == null) { MessageBox.Show("There's no track selected in iTunes. Please switch to iTunes and select some tracks.", "Selected tracks error", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }
            Properties.Settings.Default.TA += oItunes.SelectedTracks.Count;
            Properties.Settings.Default.MTA = oItunes.SelectedTracks.Count > Properties.Settings.Default.MTA ? oItunes.SelectedTracks.Count : Properties.Settings.Default.MTA;
            Properties.Settings.Default.Save();

            // Create the thread object. This does not start the thread.
            LyricsFetcherWorker lyricsFetcherWorker = new LyricsFetcherWorker(oItunes);
            lyricsFetcherWorkerThread = new Thread(lyricsFetcherWorker.DoWork);

            // Start the worker thread.
            lyricsFetcherWorkerThread.SetApartmentState(ApartmentState.MTA);
            lyricsFetcherWorkerThread.Start();
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e) {
            Credits credits = new Credits();
            credits.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void playCountSetToolStripMenuItem_Click(object sender, EventArgs e) {
            PlayCountSetting PlaySet = new PlayCountSetting();
            PlaySet.ShowDialog();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) {

        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e) {
            Settings Settings = new Settings();
            Settings.ShowDialog();
        }

        private void Abort_Click(object sender, EventArgs e) {
            Log("-- MainWorker::ButtonPressed::Pressed Abort button");
            if (lyricsFetcherWorkerThread != null && lyricsFetcherWorkerThread.IsAlive) lyricsFetcherWorkerThread.Abort();

            ThreadSafeMethods.threadSafeButtonCaptionUpdate(grab_button, "Terminating all jobs...");
            ThreadSafeMethods.threadSafeButtonEnableDisable(grab_button, false);
            ThreadSafeMethods.threadSafeButtonColorUpdate(grab_button, Color.Yellow);

            lyricsFetcherWorkerThread.Join();
        }

        private void tracksCountToolStripMenuItem_Click(object sender, EventArgs e) {
            TracksCount strTCou = new TracksCount();
            strTCou.ShowDialog();
        }

        // =================================================== ThreadSafe METHODS =================================================
        public void handleMessage(int message) {
            switch (message) {
                case FETCHING_STARTED:
                    prepareView();
                    break;
            }
        }


        // =================================================== UPDATE GUI METHODS =================================================
        public void prepareView() {
            // Table
            ThreadSafeMethods.threadSafePrepareDataGridView(dataGridView1);
            ThreadSafeMethods.threadSafeSetEnabledDataGridView(dataGridView1, false);
            // ProgressBar
            ThreadSafeMethods.threadSafeSetMaximumProgessBar(progressBar1, oItunes.SelectedTracks.Count);
            ThreadSafeMethods.threadSafeRefreshProgessBar(progressBar1);
            // Buttons
            ThreadSafeMethods.threadSafeButtonEnableDisable(Abort_button, true);
            ThreadSafeMethods.threadSafeToolStripStatusLabelUpdate(statusStrip1, "Working on it..");
            ThreadSafeMethods.threadSafeButtonEnableDisable(Abort_button, true);
            ThreadSafeMethods.threadSafeGroupBoxEnabled(table_view_groupbox, false);
            ThreadSafeMethods.threadSafeGroupBoxEnabled(current_work_groupbox, true);
            ThreadSafeMethods.threadSafeLabelUpdate(processed_tracks_value, "0");
            ThreadSafeMethods.threadSafeButtonCaptionUpdate(grab_button, "&Abort");
            ThreadSafeMethods.threadSafeButtonColorUpdate(grab_button, Color.Salmon);
            ThreadSafeMethods.threadSafeLabelUpdate(selected_tracks_value, oItunes.SelectedTracks.Count.ToString());
            ThreadSafeMethods.threadSafeLabelUpdate(remaining_tracks_value, oItunes.SelectedTracks.Count.ToString());
            ThreadSafeMethods.threadSafeSetComboBoxIndex(table_view_combobox, 0);
            // Errors management
            ThreadSafeMethods.threadSafeLabelUpdate(errors_tracks_value, "0");
            ThreadSafeMethods.threadSafeLabelColorUpdate(errors_tracks_label, Color.White);
            ThreadSafeMethods.threadSafeLabelColorUpdate(errors_tracks_value, Color.White);
            in_progress = true;
        }

        public void cleanView() {
            // Riattivo i Tasti
            ThreadSafeMethods.threadSafeGroupBoxEnabled(table_view_groupbox, true);
            ThreadSafeMethods.threadSafeButtonEnableDisable(grab_button, true);
            ThreadSafeMethods.threadSafeButtonEnableDisable(Abort_button, false);
            ThreadSafeMethods.threadSafeToolStripStatusLabelUpdate(statusStrip1, "Ready!");
            ThreadSafeMethods.threadSafeSetEnabledDataGridView(dataGridView1, true);
            ThreadSafeMethods.threadSafeButtonCaptionUpdate(grab_button, "&Grab Lyrics");
            ThreadSafeMethods.threadSafeButtonColorUpdate(grab_button, Color.Orange);
            ThreadSafeMethods.threadSafeLabelUpdate(remaining_tracks_value, "0");
            //ThreadSafeMethods.threadSafeGroupBoxEnableDisable(current_work_groupbox, false);
            // ProgressBar
            ThreadSafeMethods.threadSafeUpdateProgessBar(progressBar1, 0);
            ThreadSafeMethods.threadSafeRefreshProgessBar(progressBar1);

            // Errors gui
            if (LyricsFetcherWorker.getTotalErrors() > 0) {
                ThreadSafeMethods.threadSafeLabelUpdate(errors_tracks_value, LyricsFetcherWorker.getTotalErrors().ToString());
                errors_tracks_label.ForeColor = AppColors.DarkBlue;
                errors_tracks_value.ForeColor = AppColors.DarkBlue;
            }
            if (LyricsFetcherWorker.getProcessedTracks() == 0) {
                ThreadSafeMethods.threadSafeGroupBoxEnabled(current_work_groupbox, false);
                ThreadSafeMethods.threadSafeLabelUpdate(processed_tracks_value, "0");
            }
            in_progress = false;
        }

        public void newSongQueue(int i, String title, String artist, String album) {
            lock (tableLocker) {

                String[] data = new String[5];
                data[0] = title;
                data[1] = artist;
                data[2] = album;
                data[3] = Resources.GlobalStrings.song_status_seeking;
                data[4] = STATUS_SEEKING.ToString();
                ThreadSafeMethods.threadSafeAddRowDataGridView(dataGridView1, i + 1, data);

                /*
                dbDataSet.CurrentSessionGrabRow row = dbDataSet.CurrentSessionGrab.NewCurrentSessionGrabRow();
                row.Id = i;
                row.Title = title;
                row.Artist = artist;
                row.Album = album;
                row.Status = status;
                dbDataSet.CurrentSessionGrab.Rows.Add(row);
                currentSessionGrabTableAdapter.Update(dbDataSet.CurrentSessionGrab);
                */
            }
        }

        public void updateStatusForSong(String title, String artist, int song_index, int new_status, string error_message = "") {
            lock (tableLocker) {
                try {
                    switch (new_status) {
                        case STATUS_SEEKING:
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 4, Resources.GlobalStrings.song_status_seeking);
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 5, STATUS_SEEKING.ToString());
                            ThreadSafeMethods.threadSafeColorRowDataGridView(dataGridView1, song_index, Color.Yellow);
                            break;
                        case STATUS_TRACK_SKIPPED:
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 4, Resources.GlobalStrings.song_status_skipped);
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 5, STATUS_TRACK_SKIPPED.ToString());
                            ThreadSafeMethods.threadSafeColorRowDataGridView(dataGridView1, song_index, Color.DarkOrange);
                            break;
                        case STATUS_LYRICS_FOUND:
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 4, Resources.GlobalStrings.song_status_lyrics_found);
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 5, STATUS_LYRICS_FOUND.ToString());
                            ThreadSafeMethods.threadSafeColorRowDataGridView(dataGridView1, song_index, Color.PaleGreen);
                            break;
                        case STATUS_COMPLETED:
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 4, Resources.GlobalStrings.song_status_completed);
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 5, STATUS_COMPLETED.ToString());
                            ThreadSafeMethods.threadSafeColorRowDataGridView(dataGridView1, song_index, Color.GreenYellow);
                            break;
                        case STATUS_LYRICS_NOT_FOUND:
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 4, Resources.GlobalStrings.song_status_lyrics_not_found);
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 5, STATUS_LYRICS_NOT_FOUND.ToString());
                            ThreadSafeMethods.threadSafeColorRowDataGridView(dataGridView1, song_index, Color.OrangeRed);
                            break;
                        case STATUS_TIMEOUT:
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 4, Resources.GlobalStrings.song_status_time_out);
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 5, STATUS_TIMEOUT.ToString());
                            ThreadSafeMethods.threadSafeColorRowDataGridView(dataGridView1, song_index, Color.DarkRed);
                            break;
                        case STATUS_WEBERROR:
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 4, Resources.GlobalStrings.song_status_web_error);
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 5, STATUS_WEBERROR.ToString());
                            ThreadSafeMethods.threadSafeColorRowDataGridView(dataGridView1, song_index, Color.DarkRed);
                            break;
                        case STATUS_COMERROR:
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 4, Resources.GlobalStrings.song_status_com_error);
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 5, STATUS_COMERROR.ToString());
                            ThreadSafeMethods.threadSafeColorRowDataGridView(dataGridView1, song_index, Color.DarkRed);
                            break;
                        case STATUS_TRACK_ERROR:
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 4, Resources.GlobalStrings.song_status_track_error);
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 5, STATUS_TRACK_ERROR.ToString());
                            ThreadSafeMethods.threadSafeColorRowDataGridView(dataGridView1, song_index, Color.IndianRed);
                            break;
                        case STATUS_SENDING_LYRICS_ERROR:
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 4, Resources.GlobalStrings.song_status_lyrics_error);
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 5, STATUS_SENDING_LYRICS_ERROR.ToString());
                            ThreadSafeMethods.threadSafeColorRowDataGridView(dataGridView1, song_index, Color.PaleVioletRed);
                            break;
                        case STATUS_ABORTED:
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 4, Resources.GlobalStrings.song_status_aborted);
                            ThreadSafeMethods.threadSafeEditCellDataGridView(dataGridView1, song_index, 5, STATUS_ABORTED.ToString());
                            ThreadSafeMethods.threadSafeColorRowDataGridView(dataGridView1, song_index, Color.DimGray);
                            break;
                    }
                    Log($"-- MainWorker::UpdateStatusForSong::{title}:{artist}::{new_status}::[{error_message}]");
                    ThreadSafeMethods.threadSafeLabelUpdate(errors_tracks_value, LyricsFetcherWorker.getTotalErrors().ToString());
                }
                catch (ArgumentOutOfRangeException) {
                    // This exception is thrown when element does not exist and this is possible when Threads are so fast that
                    // the table don't manage to update itself before the thread calling, so we catch the exception and retry to
                    // update the value that, eventually, will be updated.
                    updateStatusForSong(title, artist, song_index, new_status);
                }
                catch (NullReferenceException) {

                }
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            DataGridView dgv = dataGridView1;
            using (System.IO.StreamWriter file = new System.IO.StreamWriter("table.txt", true)) {
                foreach (DataGridViewRow dgvR in dgv.Rows) {
                    for (int j = 0; j < dgv.Columns.Count; ++j) {
                        object val = dgvR.Cells[j].Value;
                        file.Write(dgvR.Cells[j].Value.ToString());
                    }
                    file.Write("\n");
                }
            }
        }

        // =================================================== MAIN FORM RELATED =================================================
        private void Main_Load(object sender, EventArgs e) {
            menuStrip1.Renderer = new ToolStripProfessionalRenderer(new MenuStripColorTable());
            menuStrip1.ForeColor = Color.White;
            total_tracks_value.Text = oItunes.LibraryPlaylist.Tracks.Count.ToString();
            table_view_combobox.SelectedIndex = 0;

            // Check for updates
            utils.Updates.CheckUpdatesAndShowDialogIfAny();
            //selected_tracks_value.Text = oItunes.SelectedTracks != null ? oItunes.SelectedTracks.Count.ToString() : "0";
            //Thread updateSelectedTracksValueThread = new Thread(this.updateSelectedTracksCountThreadDo);
            //Thread updateTotalTracksValueThread = new Thread(this.countLibraryTracksThreadDo);
            //updateSelectedTracksValueThread.Start();
            //updateTotalTracksValueThread.Start();
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e) {
            Application.Exit();
        }

        private void Main_Shown(object sender, EventArgs e) {
            if (Properties.Settings.Default.FirstStarting) {
                instructionsToolStripMenuItem_Click(null, null);
                Properties.Settings.Default.FirstStarting = false;
                Properties.Settings.Default.Save();
            }
            utils.Analytics.GetInstance().StartedApplication(Application.ProductVersion);
        }

        private void updateSelectedTracksCountThreadDo() {
            while (true) {
                try {
                    ThreadSafeMethods.threadSafeLabelUpdate(selected_tracks_value, oItunes.SelectedTracks.Count.ToString());
                }
                catch (Exception) { }
            }
        }

        private void countLibraryTracksThreadDo() {
            int i = 0;
            foreach (IITFileOrCDTrack track in oItunes.LibraryPlaylist.Tracks) {
                if (track.KindAsString.ToLower().Contains("audio")) {
                    i++;
                    ThreadSafeMethods.threadSafeLabelUpdate(total_tracks_value, i.ToString());
                }
            }
        }

        // =================================================== MENU RELATED =================================================
        private void settingsToolStripMenuItem1_Click(object sender, EventArgs e) {
            Settings Settings = new Settings();
            Settings.ShowDialog();
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e) {
            this.Close();
            Application.Exit();
        }

        private void instructionsToolStripMenuItem_Click(object sender, EventArgs e) {
            Instructions instruction_form = new Instructions();
            instruction_form.ShowDialog();
        }

        private void tracksCounterToolStripMenuItem_Click(object sender, EventArgs e) {
            TracksCount strTCou = new TracksCount();
            strTCou.ShowDialog();
        }

        private void stringRemoverToolStripMenuItem_Click(object sender, EventArgs e) {
            StringRemover strRem = new StringRemover();
            strRem.ShowDialog();
        }

        private void playCountSetterToolStripMenuItem_Click(object sender, EventArgs e) {
            PlayCountSetting PlaySet = new PlayCountSetting();
            PlaySet.ShowDialog();
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e) {
            Credits credits = new Credits();
            credits.ShowDialog();
        }

        private void table_view_combobox_SelectedIndexChanged(object sender, EventArgs e) {
            updateFilterOnTable();
        }

        private void updateFilterOnTable() {
            switch (table_view_combobox.SelectedIndex) {
                case 0: // All
                    foreach (DataGridViewRow r in dataGridView1.Rows) {
                        r.Visible = true;
                    }
                    break;
                case 1: // Not Found
                    foreach (DataGridViewRow r in dataGridView1.Rows) {
                        if (Convert.ToInt32(r.Cells[5].Value) == STATUS_LYRICS_NOT_FOUND) r.Visible = true;
                        else r.Visible = false;
                    }
                    break;
                case 2: // Errors
                    foreach (DataGridViewRow r in dataGridView1.Rows) {
                        int value = Convert.ToInt32(r.Cells[5].Value);
                        if (value == STATUS_TIMEOUT || value == STATUS_COMERROR || value == STATUS_SENDING_LYRICS_ERROR || value == STATUS_WEBERROR || value == STATUS_TRACK_ERROR) r.Visible = true;
                        else r.Visible = false;
                    }
                    break;
                case 3: // Completed
                    foreach (DataGridViewRow r in dataGridView1.Rows) {
                        if (Convert.ToInt32(r.Cells[5].Value) == STATUS_COMPLETED) r.Visible = true;
                        else r.Visible = false;
                    }
                    break;
                case 4: // Seeking
                    foreach (DataGridViewRow r in dataGridView1.Rows) {
                        if (Convert.ToInt32(r.Cells[5].Value) == STATUS_SEEKING) r.Visible = true;
                        else r.Visible = false;
                    }
                    break;
                default:
                    break;
            }
        }

        private void ID3TagForcerToolStripMenuItem1_Click(object sender, EventArgs e) {
            ID3TagForcer ID3TagForcer = new ID3TagForcer();
            ID3TagForcer.ShowDialog();
        }

    }
}