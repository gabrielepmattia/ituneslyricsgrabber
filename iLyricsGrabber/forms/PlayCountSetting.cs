﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using iTunesLib;

namespace iLyricsGrabber
{
    public partial class PlayCountSetting : Form
    {

        // Create the itunes object
        iTunesLib.iTunesApp oItunes = new iTunesLib.iTunesApp();
        public PlayCountSetting()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            // Create selected tracks object
            iTunesLib.IITTrackCollection selected_tracks = oItunes.SelectedTracks;

            // Verifico se le tracce selezionate sono più di una
            if (selected_tracks.Count > 1) {
                MessageBox.Show("Select only 1 track please!");

            } else {
                //Attivo il gruppo
                selectedtracks_group.Enabled = true;

            foreach (IITFileOrCDTrack track in selected_tracks)
                {
                    // Mostro i dati relativi alla traccia selezionata
                    title_selected.Text = track.Name;
                    played_selected.Text = track.PlayedCount.ToString();

                    // Attivo il secondo groupbox
                    custom_groupbox.Enabled = true;
                 }

            }        
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Prendo il valore nella textbox
            int custom_value = Int32.Parse(custompc_textbox.Text);

            // Creo l'oggetto delle tracce selezionate
            iTunesLib.IITTrackCollection selected_tracks = oItunes.SelectedTracks;

            // Applico il numero desiderato
             foreach (IITFileOrCDTrack track in selected_tracks)
                {
                    track.PlayedCount = custom_value;
                }
             result.Text = "Done!";
        }
    }
}
