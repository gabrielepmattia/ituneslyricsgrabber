﻿namespace iLyricsGrabber
{
    partial class Main
    {

        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusbar_label = new System.Windows.Forms.ToolStripStatusLabel();
            this.statubar_label = new System.Windows.Forms.ToolStripStatusLabel();
            this.grab_button = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Artist = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Album = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Abort_button = new System.Windows.Forms.Button();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripDropDownButton3 = new System.Windows.Forms.ToolStripDropDownButton();
            this.button1 = new System.Windows.Forms.Button();
            this.total_tracks_label = new System.Windows.Forms.Label();
            this.total_tracks_value = new System.Windows.Forms.Label();
            this.library_groupbox = new System.Windows.Forms.GroupBox();
            this.selected_tracks_label = new System.Windows.Forms.Label();
            this.selected_tracks_value = new System.Windows.Forms.Label();
            this.current_work_groupbox = new System.Windows.Forms.GroupBox();
            this.errors_tracks_value = new System.Windows.Forms.Label();
            this.errors_tracks_label = new System.Windows.Forms.Label();
            this.remaining_tracks_value = new System.Windows.Forms.Label();
            this.remaining_tracks_label = new System.Windows.Forms.Label();
            this.processed_tracks_label = new System.Windows.Forms.Label();
            this.processed_tracks_value = new System.Windows.Forms.Label();
            this.table_view_groupbox = new System.Windows.Forms.GroupBox();
            this.table_view_combobox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tracksCounterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stringRemoverToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.playCountSetterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ID3TagForcerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instructionsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.library_groupbox.SuspendLayout();
            this.current_work_groupbox.SuspendLayout();
            this.table_view_groupbox.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.Transparent;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusbar_label});
            resources.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            // 
            // statusbar_label
            // 
            this.statusbar_label.ForeColor = System.Drawing.Color.White;
            this.statusbar_label.Name = "statusbar_label";
            resources.ApplyResources(this.statusbar_label, "statusbar_label");
            // 
            // statubar_label
            // 
            this.statubar_label.Name = "statubar_label";
            resources.ApplyResources(this.statubar_label, "statubar_label");
            // 
            // grab_button
            // 
            resources.ApplyResources(this.grab_button, "grab_button");
            this.grab_button.BackColor = System.Drawing.Color.Orange;
            this.grab_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.grab_button.FlatAppearance.BorderSize = 0;
            this.grab_button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.grab_button.ForeColor = System.Drawing.Color.Black;
            this.grab_button.Name = "grab_button";
            this.grab_button.UseVisualStyleBackColor = false;
            this.grab_button.Click += new System.EventHandler(this.grab_button_Click);
            // 
            // progressBar1
            // 
            resources.ApplyResources(this.progressBar1, "progressBar1");
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Step = 1;
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            resources.ApplyResources(this.dataGridView1, "dataGridView1");
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Title,
            this.Artist,
            this.Album,
            this.Status,
            this.StatusCode});
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Orange;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.RowTemplate.Height = 16;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // ID
            // 
            this.ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ID.FillWeight = 40F;
            resources.ApplyResources(this.ID, "ID");
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // Title
            // 
            this.Title.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Title.FillWeight = 93.27411F;
            resources.ApplyResources(this.Title, "Title");
            this.Title.Name = "Title";
            this.Title.ReadOnly = true;
            // 
            // Artist
            // 
            this.Artist.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Artist.FillWeight = 93.27411F;
            resources.ApplyResources(this.Artist, "Artist");
            this.Artist.Name = "Artist";
            this.Artist.ReadOnly = true;
            // 
            // Album
            // 
            this.Album.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Album.FillWeight = 93.27411F;
            resources.ApplyResources(this.Album, "Album");
            this.Album.Name = "Album";
            this.Album.ReadOnly = true;
            // 
            // Status
            // 
            this.Status.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Status.FillWeight = 93.27411F;
            resources.ApplyResources(this.Status, "Status");
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            // 
            // StatusCode
            // 
            resources.ApplyResources(this.StatusCode, "StatusCode");
            this.StatusCode.Name = "StatusCode";
            this.StatusCode.ReadOnly = true;
            // 
            // Abort_button
            // 
            this.Abort_button.BackColor = System.Drawing.Color.Salmon;
            resources.ApplyResources(this.Abort_button, "Abort_button");
            this.Abort_button.FlatAppearance.BorderSize = 0;
            this.Abort_button.Name = "Abort_button";
            this.Abort_button.UseVisualStyleBackColor = false;
            this.Abort_button.Click += new System.EventHandler(this.Abort_Click);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            resources.ApplyResources(this.toolStripDropDownButton1, "toolStripDropDownButton1");
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            resources.ApplyResources(this.toolStripDropDownButton2, "toolStripDropDownButton2");
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            // 
            // toolStripDropDownButton3
            // 
            this.toolStripDropDownButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            resources.ApplyResources(this.toolStripDropDownButton3, "toolStripDropDownButton3");
            this.toolStripDropDownButton3.Name = "toolStripDropDownButton3";
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // total_tracks_label
            // 
            resources.ApplyResources(this.total_tracks_label, "total_tracks_label");
            this.total_tracks_label.BackColor = System.Drawing.Color.Transparent;
            this.total_tracks_label.ForeColor = System.Drawing.Color.White;
            this.total_tracks_label.Name = "total_tracks_label";
            // 
            // total_tracks_value
            // 
            resources.ApplyResources(this.total_tracks_value, "total_tracks_value");
            this.total_tracks_value.BackColor = System.Drawing.Color.Transparent;
            this.total_tracks_value.ForeColor = System.Drawing.Color.Transparent;
            this.total_tracks_value.Name = "total_tracks_value";
            // 
            // library_groupbox
            // 
            resources.ApplyResources(this.library_groupbox, "library_groupbox");
            this.library_groupbox.BackColor = System.Drawing.Color.Transparent;
            this.library_groupbox.Controls.Add(this.selected_tracks_label);
            this.library_groupbox.Controls.Add(this.selected_tracks_value);
            this.library_groupbox.Controls.Add(this.total_tracks_label);
            this.library_groupbox.Controls.Add(this.total_tracks_value);
            this.library_groupbox.ForeColor = System.Drawing.Color.White;
            this.library_groupbox.Name = "library_groupbox";
            this.library_groupbox.TabStop = false;
            // 
            // selected_tracks_label
            // 
            resources.ApplyResources(this.selected_tracks_label, "selected_tracks_label");
            this.selected_tracks_label.BackColor = System.Drawing.Color.Transparent;
            this.selected_tracks_label.ForeColor = System.Drawing.Color.White;
            this.selected_tracks_label.Name = "selected_tracks_label";
            // 
            // selected_tracks_value
            // 
            resources.ApplyResources(this.selected_tracks_value, "selected_tracks_value");
            this.selected_tracks_value.BackColor = System.Drawing.Color.Transparent;
            this.selected_tracks_value.ForeColor = System.Drawing.Color.Transparent;
            this.selected_tracks_value.Name = "selected_tracks_value";
            // 
            // current_work_groupbox
            // 
            resources.ApplyResources(this.current_work_groupbox, "current_work_groupbox");
            this.current_work_groupbox.BackColor = System.Drawing.Color.Transparent;
            this.current_work_groupbox.Controls.Add(this.errors_tracks_value);
            this.current_work_groupbox.Controls.Add(this.errors_tracks_label);
            this.current_work_groupbox.Controls.Add(this.remaining_tracks_value);
            this.current_work_groupbox.Controls.Add(this.remaining_tracks_label);
            this.current_work_groupbox.Controls.Add(this.progressBar1);
            this.current_work_groupbox.Controls.Add(this.processed_tracks_label);
            this.current_work_groupbox.Controls.Add(this.processed_tracks_value);
            this.current_work_groupbox.ForeColor = System.Drawing.Color.White;
            this.current_work_groupbox.Name = "current_work_groupbox";
            this.current_work_groupbox.TabStop = false;
            // 
            // errors_tracks_value
            // 
            resources.ApplyResources(this.errors_tracks_value, "errors_tracks_value");
            this.errors_tracks_value.Name = "errors_tracks_value";
            // 
            // errors_tracks_label
            // 
            resources.ApplyResources(this.errors_tracks_label, "errors_tracks_label");
            this.errors_tracks_label.Name = "errors_tracks_label";
            // 
            // remaining_tracks_value
            // 
            resources.ApplyResources(this.remaining_tracks_value, "remaining_tracks_value");
            this.remaining_tracks_value.Name = "remaining_tracks_value";
            // 
            // remaining_tracks_label
            // 
            resources.ApplyResources(this.remaining_tracks_label, "remaining_tracks_label");
            this.remaining_tracks_label.Name = "remaining_tracks_label";
            // 
            // processed_tracks_label
            // 
            resources.ApplyResources(this.processed_tracks_label, "processed_tracks_label");
            this.processed_tracks_label.BackColor = System.Drawing.Color.Transparent;
            this.processed_tracks_label.ForeColor = System.Drawing.Color.White;
            this.processed_tracks_label.Name = "processed_tracks_label";
            // 
            // processed_tracks_value
            // 
            resources.ApplyResources(this.processed_tracks_value, "processed_tracks_value");
            this.processed_tracks_value.BackColor = System.Drawing.Color.Transparent;
            this.processed_tracks_value.ForeColor = System.Drawing.Color.Transparent;
            this.processed_tracks_value.Name = "processed_tracks_value";
            // 
            // table_view_groupbox
            // 
            resources.ApplyResources(this.table_view_groupbox, "table_view_groupbox");
            this.table_view_groupbox.BackColor = System.Drawing.Color.Transparent;
            this.table_view_groupbox.Controls.Add(this.table_view_combobox);
            this.table_view_groupbox.Controls.Add(this.label2);
            this.table_view_groupbox.ForeColor = System.Drawing.Color.White;
            this.table_view_groupbox.Name = "table_view_groupbox";
            this.table_view_groupbox.TabStop = false;
            // 
            // table_view_combobox
            // 
            this.table_view_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.table_view_combobox, "table_view_combobox");
            this.table_view_combobox.FormattingEnabled = true;
            this.table_view_combobox.Items.AddRange(new object[] {
            resources.GetString("table_view_combobox.Items"),
            resources.GetString("table_view_combobox.Items1"),
            resources.GetString("table_view_combobox.Items2"),
            resources.GetString("table_view_combobox.Items3"),
            resources.GetString("table_view_combobox.Items4")});
            this.table_view_combobox.Name = "table_view_combobox";
            this.table_view_combobox.SelectedIndexChanged += new System.EventHandler(this.table_view_combobox_SelectedIndexChanged);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Name = "label2";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.aboutToolStripMenuItem});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem1,
            this.exitToolStripMenuItem1});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            resources.ApplyResources(this.fileToolStripMenuItem, "fileToolStripMenuItem");
            // 
            // settingsToolStripMenuItem1
            // 
            this.settingsToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.settingsToolStripMenuItem1.Name = "settingsToolStripMenuItem1";
            resources.ApplyResources(this.settingsToolStripMenuItem1, "settingsToolStripMenuItem1");
            this.settingsToolStripMenuItem1.Click += new System.EventHandler(this.settingsToolStripMenuItem1_Click);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            resources.ApplyResources(this.exitToolStripMenuItem1, "exitToolStripMenuItem1");
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tracksCounterToolStripMenuItem,
            this.stringRemoverToolStripMenuItem1,
            this.playCountSetterToolStripMenuItem,
            this.ID3TagForcerToolStripMenuItem1});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            resources.ApplyResources(this.toolsToolStripMenuItem, "toolsToolStripMenuItem");
            // 
            // tracksCounterToolStripMenuItem
            // 
            this.tracksCounterToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.tracksCounterToolStripMenuItem.Name = "tracksCounterToolStripMenuItem";
            resources.ApplyResources(this.tracksCounterToolStripMenuItem, "tracksCounterToolStripMenuItem");
            this.tracksCounterToolStripMenuItem.Click += new System.EventHandler(this.tracksCounterToolStripMenuItem_Click);
            // 
            // stringRemoverToolStripMenuItem1
            // 
            this.stringRemoverToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.stringRemoverToolStripMenuItem1.Name = "stringRemoverToolStripMenuItem1";
            resources.ApplyResources(this.stringRemoverToolStripMenuItem1, "stringRemoverToolStripMenuItem1");
            this.stringRemoverToolStripMenuItem1.Click += new System.EventHandler(this.stringRemoverToolStripMenuItem_Click);
            // 
            // playCountSetterToolStripMenuItem
            // 
            this.playCountSetterToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.playCountSetterToolStripMenuItem.Name = "playCountSetterToolStripMenuItem";
            resources.ApplyResources(this.playCountSetterToolStripMenuItem, "playCountSetterToolStripMenuItem");
            this.playCountSetterToolStripMenuItem.Click += new System.EventHandler(this.playCountSetterToolStripMenuItem_Click);
            // 
            // ID3TagForcerToolStripMenuItem1
            // 
            this.ID3TagForcerToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.ID3TagForcerToolStripMenuItem1.Name = "ID3TagForcerToolStripMenuItem1";
            resources.ApplyResources(this.ID3TagForcerToolStripMenuItem1, "ID3TagForcerToolStripMenuItem1");
            this.ID3TagForcerToolStripMenuItem1.Click += new System.EventHandler(this.ID3TagForcerToolStripMenuItem1_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.instructionsToolStripMenuItem1,
            this.toolStripSeparator3,
            this.aboutToolStripMenuItem1});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            resources.ApplyResources(this.aboutToolStripMenuItem, "aboutToolStripMenuItem");
            // 
            // instructionsToolStripMenuItem1
            // 
            this.instructionsToolStripMenuItem1.BackColor = System.Drawing.SystemColors.Control;
            this.instructionsToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.instructionsToolStripMenuItem1.Name = "instructionsToolStripMenuItem1";
            resources.ApplyResources(this.instructionsToolStripMenuItem1, "instructionsToolStripMenuItem1");
            this.instructionsToolStripMenuItem1.Click += new System.EventHandler(this.instructionsToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
            // 
            // aboutToolStripMenuItem1
            // 
            this.aboutToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.aboutToolStripMenuItem1.Name = "aboutToolStripMenuItem1";
            resources.ApplyResources(this.aboutToolStripMenuItem1, "aboutToolStripMenuItem1");
            this.aboutToolStripMenuItem1.Click += new System.EventHandler(this.aboutToolStripMenuItem1_Click);
            // 
            // Main
            // 
            this.AcceptButton = this.grab_button;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::iLyricsGrabber.Properties.Resources.iTLG_bg;
            this.Controls.Add(this.table_view_groupbox);
            this.Controls.Add(this.current_work_groupbox);
            this.Controls.Add(this.library_groupbox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Abort_button);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.grab_button);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.Load += new System.EventHandler(this.Main_Load);
            this.Shown += new System.EventHandler(this.Main_Shown);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.library_groupbox.ResumeLayout(false);
            this.library_groupbox.PerformLayout();
            this.current_work_groupbox.ResumeLayout(false);
            this.current_work_groupbox.PerformLayout();
            this.table_view_groupbox.ResumeLayout(false);
            this.table_view_groupbox.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statubar_label;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.Button grab_button;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton3;
        private System.Windows.Forms.ToolStripStatusLabel statusbar_label;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button Abort_button;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label total_tracks_label;
        private System.Windows.Forms.Label total_tracks_value;
        private System.Windows.Forms.GroupBox library_groupbox;
        private System.Windows.Forms.Label selected_tracks_label;
        private System.Windows.Forms.Label selected_tracks_value;
        private System.Windows.Forms.GroupBox current_work_groupbox;
        private System.Windows.Forms.Label processed_tracks_label;
        private System.Windows.Forms.Label processed_tracks_value;
        private System.Windows.Forms.GroupBox table_view_groupbox;
        private System.Windows.Forms.ComboBox table_view_combobox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tracksCounterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stringRemoverToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem playCountSetterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem instructionsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Label remaining_tracks_value;
        private System.Windows.Forms.Label remaining_tracks_label;
        private System.Windows.Forms.Label errors_tracks_value;
        private System.Windows.Forms.Label errors_tracks_label;
        private System.Windows.Forms.ToolStripMenuItem ID3TagForcerToolStripMenuItem1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Title;
        private System.Windows.Forms.DataGridViewTextBoxColumn Artist;
        private System.Windows.Forms.DataGridViewTextBoxColumn Album;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn StatusCode;
    }
}

